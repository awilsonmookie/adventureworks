﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AdventureWorks.Models
{
    public class ContactModel
    {
        [Required]
        [Display(Name = "Contact Name")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Display(Name = "Contact Area")]
        public int SelectedContactArea { get; set; }
        public SelectList ContactArea { get; set; }

        [Required]
        [Display(Name = "Comment/Request Information")]
        public string ContactText { get; set; }
    }
}