﻿using System.Data.Entity;


namespace AdventureWorks.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext() : base("UserSecurity"){}

        public DbSet<UserProfile> UserProfiles { get; set; }
    }
}