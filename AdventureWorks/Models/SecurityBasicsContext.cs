﻿
using System.Data.Entity;


namespace AdventureWorks.Models
{
    public class SecurityBasicsContext : DbContext
    {
        public SecurityBasicsContext() : base("AdventureWorks") { }

        public DbSet<UserProfile> UserProfiles { get; set; }
    }
}